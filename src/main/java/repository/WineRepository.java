package repository;

import org.fluttercode.datafactory.impl.DataFactory;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Random;
import java.util.UUID;

public class WineRepository {

    HashSet<UUID> wines = new HashSet<>();
    private DataFactory dataFactory;

    private static WineRepository instance;

    public static WineRepository getInstance() {
        if (instance == null){
            instance = new WineRepository();
        }
        return instance;
    }

    private WineRepository(){
        dataFactory = new DataFactory();
    }

    public Wine getNewWine() {
        Wine wine = generateRandomWine();
        return wine;
    }

    private Wine generateRandomWine() {
        Wine wine = new Wine();
        wine.setAlcohol((float) dataFactory.getNumberBetween(80, 142) / 10);
        wine.setBottleCapacityInMilliliters(dataFactory.getNumberBetween(500, 2000));
        wine.setPrice(new BigDecimal(dataFactory.getNumberBetween(1234, 12345) / 100));
        wine.setName(dataFactory.getRandomWord(4, 6) + " " + dataFactory.getRandomWord(6, 10));
        return wine;
    }

    public Wine getUpdatedWine() {
        Wine wine = generateRandomWine();
        wine.setId(getRandomWineId());
        return wine;
    }

    public UUID getRandomWineId() {
        int size = wines.size();
        int item = new Random().nextInt(size);
        int i = 0;

        for(UUID id : wines)
        {
            if (i == item)
                return id;
            i++;
        }

        return null;
    }

    public void deleteWine(UUID id) {
        wines.remove(id);
    }

    public void registerWineId(UUID id) {
        wines.add(id);
    }


    public int getWinesQuantity() {
        return wines.size();
    }
}
