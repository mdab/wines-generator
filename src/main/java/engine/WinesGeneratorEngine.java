package engine;

import generator.*;
import org.apache.log4j.LogManager;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.util.*;

import org.apache.log4j.Logger;
import repository.Wine;
import repository.WineRepository;

@Service
public class WinesGeneratorEngine {
    private final String resourceUrl;
    private int requestCount;
    private int errorCount;
    private long currentTime;
    private long logInterval;

    private Map<String, RequestAbstractGenerator> generators;
    private Random randomNumber;

    static Logger log = LogManager.getLogger("Wines Generator logger");
    private WineRepository winesRepository;

    public WinesGeneratorEngine(RestTemplateBuilder restTemplateBuilder, String resourceUrl) {

        this.resourceUrl = resourceUrl;
        this.logInterval = 60000;

        resetCounters();

        generators = new HashMap<>();
        generators.put("POST", new PostGenerator(resourceUrl));
        generators.put("GET", new GetGenerator(resourceUrl));
        generators.put("PUT", new PutGenerator(resourceUrl));
        generators.put("DELETE", new DeleteGenerator(resourceUrl));

        randomNumber = new Random();

        winesRepository =  WineRepository.getInstance();
    }

    private void resetCounters() {
        this.requestCount = 0;
        this.currentTime= 0;
        this.errorCount = 0;
    }

    public void runWinesGenerator(int period) {

        log.info("Generating random requests for " + period + " seconds.");

        long startTime = System.currentTimeMillis();
        long nextCheck = logInterval;

        postWines(10);

        Wine wine = null;

        while (true){

            currentTime = System.currentTimeMillis() - startTime;

            if(currentTime > nextCheck ){
                nextCheck = nextCheck + logInterval;
                logCurrentResult();
            }

            generateRandomRequest();

            if(currentTime >= (period * 1000) ){
                break;
            }

            if(winesRepository.getWinesQuantity() == 0){
                postWines(10);
            }
        }

        resetCounters();
        log.info("Finished generating random requests for " + period + " seconds.");
    }

    private void generateRandomRequest() {

        ArrayList<RequestAbstractGenerator> generatorsList = new ArrayList<>(generators.values());
        RequestAbstractGenerator randomRequestAbstractGenerator = generatorsList.get(randomNumber.nextInt(generatorsList.size()));
        Wine wine;

        if(randomRequestAbstractGenerator.getRequestTypeName().equals("POST")){
            randomRequestAbstractGenerator.generateRequest(winesRepository.getNewWine());
        } else if (randomRequestAbstractGenerator.getRequestTypeName().equals("PUT")){
            randomRequestAbstractGenerator.generateRequest(winesRepository.getUpdatedWine());
        } else if (randomRequestAbstractGenerator.getRequestTypeName().equals("DELETE")){
            wine = new Wine();
            wine.setId(winesRepository.getRandomWineId());
            randomRequestAbstractGenerator.generateRequest(wine);
            winesRepository.deleteWine(wine.getId());
        } else {
            wine = new Wine();
            wine.setId(winesRepository.getRandomWineId());
            randomRequestAbstractGenerator.generateRequest(wine);
        }
    }


    private void postWines(int n) {
        for (int i = 0; i < n; i++) {
            Wine wine = winesRepository.getNewWine();
            winesRepository.registerWineId(generators.get("POST").generateRequest(wine));
        }
    }

    private void logCurrentResult() {

        int requestCount = RequestAbstractGenerator.getRequestCount();
        int errorCount = RequestAbstractGenerator.getErrorCount();

        log.info("Sent " + requestCount + " POST request within " + currentTime / 1000 + " seconds.");
        log.info("Success ratio: " + ((requestCount-errorCount) / requestCount) * 100 + " %");
        log.info("Speed: " + (float) requestCount / ((float)currentTime / 1000.00) + " rps");
    }

}