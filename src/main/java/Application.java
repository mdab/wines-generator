import engine.WinesGeneratorEngine;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"pl.ftims.pl.wines-generator"})
    public class Application {
        public static void main(String[] args) {
            String url = args[0];
            int generationTime = Integer.parseInt(args[1]);

            WinesGeneratorEngine generator = new WinesGeneratorEngine(new RestTemplateBuilder(), url);
            generator.runWinesGenerator(generationTime);
            SpringApplication.run(Application.class, args);
        }
    }
