package generator;

import repository.Wine;

import java.util.UUID;

public class GetGenerator extends RequestAbstractGenerator {
    public GetGenerator(String resourceUrl) {
        super();
        this.resourceUrl = resourceUrl + "/api/wines/";
    }

    @Override
    public String getRequestTypeName() {
        return "GET";
    }

    @Override
    protected UUID performRequest(Wine wine) {
        return restTemplate.getForObject(resourceUrl+wine.getId(), Wine.class).getId();
    }
}
