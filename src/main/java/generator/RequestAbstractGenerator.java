package generator;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import repository.Wine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;


public abstract class RequestAbstractGenerator {
    private static int requestCount;
    private static int errorCount;
    protected final RestTemplate restTemplate;
    private static Logger log;
    protected String resourceUrl;

    public RequestAbstractGenerator() {
        this.restTemplate = new RestTemplateBuilder().build(); //?
        this.log = LogManager.getLogger("Wines Generator logger");
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON));
        messageConverters.add(converter);
        this.restTemplate.setMessageConverters(messageConverters);
    }

    public static int getRequestCount() {
        return requestCount;
    }

    public static int getErrorCount() {
        return errorCount;
    }

    public UUID generateRequest(Wine wine){
        try {
            log.debug(getRequestTypeName() + " " + wine);
            UUID id = performRequest(wine);
            log.debug(getRequestTypeName() + wine + " successed.");
            requestCount++;

            return id;
        } catch (RestClientException exception) {
            log.error("Error while " + getRequestTypeName() + " " + wine + " due to " + exception);
            errorCount++;
        }

        return null;
    }

    public abstract String getRequestTypeName();

    protected abstract UUID performRequest(Wine wine);

}
