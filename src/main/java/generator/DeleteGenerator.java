package generator;

import repository.Wine;

import java.util.UUID;

public class DeleteGenerator extends RequestAbstractGenerator {
    public DeleteGenerator(String resourceUrl) {
        super();
        this.resourceUrl = resourceUrl + "/api/wines/";
    }

    @Override
    public String getRequestTypeName() {
        return "DELETE";
    }

    @Override
    protected UUID performRequest(Wine wine) {
        restTemplate.delete(resourceUrl+wine.getId());

        return wine.getId();
    }
}
