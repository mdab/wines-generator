package generator;

import repository.Wine;

import java.util.UUID;

public class PostGenerator extends RequestAbstractGenerator {
    public PostGenerator(String resourceUrl) {
        super();
        this.resourceUrl = resourceUrl + "/api/wines/create";
    }

    @Override
    public String getRequestTypeName() {
        return "POST";
    }

    @Override
    protected UUID performRequest(Wine wine) {
        return restTemplate.postForObject(resourceUrl, wine, Wine.class).getId();
    }

}
