package generator;

import repository.Wine;

import java.util.UUID;

public class PutGenerator extends RequestAbstractGenerator {
    public PutGenerator(String resourceUrl) {
        super();
        this.resourceUrl = resourceUrl + "/api/wines/";
    }

    @Override
    public String getRequestTypeName() {
        return "PUT";
    }

    @Override
    protected UUID performRequest(Wine wine) {

        restTemplate.put(resourceUrl+wine.getId(), wine, Wine.class);

        return wine.getId();
    }
}
