FROM openjdk:8-jre
MAINTAINER Marcin Dabrowski <m.dab@hotmail.com>
ENV SERVICE_URL=http://wines-catalog.default.svc.cluster.local
ENV GENERATION_TIME=10

ENTRYPOINT /usr/bin/java -jar /usr/share/wines/wines-generator.jar $SERVICE_URL $GENERATION_TIME
ARG JAR_FILE
ADD target/${JAR_FILE} /usr/share/wines/wines-generator.jar